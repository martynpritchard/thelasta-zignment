#include "Player.h"
#include "Animation.h"
#include "Vector2f.h"
#include "TextureUtils.h"
#include "Game.h"
#include "AABB.h"

#include <stdexcept>
#include <string>

using std::string;

// Only types with a fixed bit representaition can be
// defined in the header file
const float Player::COOLDOWN_MAX = 0.2f;

/**
 * Player
 * 
 * Constructor, setup all the simple stuff. Set pointers to null etc. 
 *  
 */
Player::Player() : Sprite()
{
    state = IDLE;
    // Speed it low to make it the player have better accuracy when shooting.
    speed = 65.0f;

    // Size of Player sprite
    targetRectangle.w = SPRITE_WIDTH *1.7;
    targetRectangle.h = SPRITE_HEIGHT *1.7;

    // Initialise weapon cooldown. 
    cooldownTimer = 1;

    // Init points
    points = 0;
    health = 3;
}

/**
 * initPlayer
 * 
 * Function to populate an animation structure from given paramters. 
 *  
 * @param renderer Target SDL_Renderer to use for optimisation.
 * @exception Throws an exception on file not found or out of memory. 
 */
void Player::init(SDL_Renderer *renderer)
{
    // path string - Survivor Image
    string path("assets/images/Survivor.png");

    // Position
    Vector2f position(200.0f,200.0f);

    // Call sprite constructor
    Sprite::init(renderer, path, 5, &position);

    // Animation set-up

    for (int i = 0; i < maxAnimations; i++)
    {
        animations[i]->setMaxFrameTime(0.2f);
    }

    aabb = new AABB(this->getPosition(), SPRITE_HEIGHT, SPRITE_WIDTH);

}

float Player::calculateOrientation(Vector2f* direction)
{
    float angle = atan2f(direction->getY(), direction->getX()); //get angle (0,2pi)
    angle *= (180.0f / 3.142f); //convert to degrees
    return angle + angleOffset;
}

void Player::draw(SDL_Renderer* renderer)
{
    // Get current animation based on the state.
    Animation* current = this->animations[getCurrentAnimationState()];

    //SDL_RenderCopy(renderer, texture, current->getCurrentFrame(), &targetRectangle);
    SDL_RenderCopyEx(renderer, texture, current->getCurrentFrame(), &targetRectangle, orientation, nullptr, SDL_FLIP_NONE);
}

/**
 * ~Player
 * 
 * Destroys the player and any associated 
 * objects 
 * 
 */
Player::~Player()
{

}

void Player::update(float timeDeltaInSeconds)
{
    Sprite::update(timeDeltaInSeconds);
    cooldownTimer += timeDeltaInSeconds;
    hitcooldown += timeDeltaInSeconds;
}

/**
 * processInput
 * 
 * Method to process inputs for the player 
 * Note: Need to think about other forms of input!
 * 
 * @param keyStates The keystates array. 
 */
void Player::processInput(const Uint8 *keyStates)
{
    // Process Player Input

    float verticalInput = 0.0f;
    float horizontalInput = 0.0f;

    // If no keys are being pressed then no animation should occur
    state = IDLE;

    // Set of or each Keypress - UP key = up, SPACE Key = shoot etc.
    if (keyStates[SDL_SCANCODE_UP])
    {
        verticalInput = -1.0f;
        state = UP;
    }

    if (keyStates[SDL_SCANCODE_DOWN])
    {
        verticalInput = 1.0f;
        state = DOWN;
    }

    if (keyStates[SDL_SCANCODE_RIGHT])
    {
        horizontalInput = 1.0f;
        state = RIGHT;
    }

    if (keyStates[SDL_SCANCODE_LEFT])
    {
        horizontalInput = -1.0f;
        state = LEFT;
    }

    // Calculate player velocity.
    velocity->setX(horizontalInput);
    velocity->setY(verticalInput);
    velocity->normalise();
    velocity->scale(speed);

    if (keyStates[SDL_SCANCODE_SPACE])
    {
        fire();
    }
}

int Player::getCurrentAnimationState()
{
    return state;
}

void Player::setGame(Game* game)
{
    this->game = game;
}


void Player::fire()
{   
    // Need a cooldown timer, otherwise we shoot 
    // a million bullets a second and our npc
    // dies instantly
    if(cooldownTimer > COOLDOWN_MAX)
    {
        // Can't fire in idle state!
        if(velocity->length() > 0.0f)
        {
            game->createBullet(position,velocity);
            cooldownTimer = 0;
        }
    }   
}

void Player::addHealth(int health)
{
    this->health += health;
}

void Player::subHealth(int health)
{
    if (hitcooldown > COOLDOWN_MAX)
    {
        this->health -= health;
        hitcooldown = 0;
    }

}

int Player::getHealth()
{
    return health;
}


void Player::addScore(int points)
{
    this->points += points;
}

int Player::getScore()
{
    return points;
}

bool Player::playerisDead()
{
    if (health > 0)
        return false;
    else
        return true;
}