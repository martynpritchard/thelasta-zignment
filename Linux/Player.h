#ifndef PLAYER_H_
#define PLAYER_H_

#include "SDL2Common.h"
#include "Sprite.h"
#include "Game.h"

// Forward declerations
// improve compile time. 
class Vector2f;
class Animation;
class Game;

class Player : public Sprite
{
private:
    
    // Animation state
    int state;
        
    // Sprite information
    static const int SPRITE_HEIGHT = 32;
    static const int SPRITE_WIDTH = 24;

    // Bullet spawn
    float hitcooldown;
    float cooldownTimer;
    static const float COOLDOWN_MAX;

    // angle in radians
    float orientation;

    // don't know the direction of the sprite
// in the image I have?
    float angleOffset;
    float calculateOrientation(Vector2f* direction);


    Game* game;

    // Score and Health
    int points;
    int health;
public:
    Player();
    ~Player();

    // Player Animation states
    enum PlayerState{LEFT=0, RIGHT, UP, DOWN, IDLE};
    
    void init(SDL_Renderer *renderer);
    void processInput(const Uint8 *keyStates);

    void setGame(Game* game);
    void fire();

    int getCurrentAnimationState();

    void draw(SDL_Renderer* renderer);

    //Overloads
    void update(float timeDeltaInSeconds);

    void addScore(int points);
    int getScore();

    bool playerisDead();

    void subHealth(int points);
    void addHealth(int points);
    int getHealth();

};



#endif